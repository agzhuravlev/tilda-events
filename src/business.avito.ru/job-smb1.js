$(document).ready(function () {
  const landingSlug = 'job-smb1';

  if (window.sendPageViewEvent) {
    window.sendPageViewEvent(landingSlug);
  }

  if (window.sendClickEvent) {
    $('[data-elem-type="button"] > a:contains("Разместить вакансию")').one('click', function() {
      window.sendClickEvent(landingSlug, 'Нажатие на кнопку', 'Нажатие на кнопку "Разместить вакансию" бабл');
    });

    $('.tn-atom__sbs-anim-wrapper > a:contains("Разместить вакансию")').one('click', function(e) {
      window.sendClickEvent(landingSlug, 'Нажатие на кнопку', 'Нажатие на кнопку "Разместить вакансию" ссылка');
    });

    $('.tn-atom__sbs-anim-wrapper > a:contains("Посмотреть резюме")').one('click', function() {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на кнопку "Посмотреть резюме"');
    });

    $('a:contains("Написать службе поддержки")').one('click', function() {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на кнопку "Написать службе поддержки"');
    });

    $('a[href="https://t.me/avito_jobs"]').one('click', function() {
      window.sendClickEvent(landingSlug, 'Переход в социальные сети', 'Переход в социальные сети "Телеграм"');
    });

    $('a[href="https://www.youtube.com/channel/UCSBD3YinrigmZRCyNpHMYAQ"]').one('click', function() {
      window.sendClickEvent(landingSlug, 'Переход в социальные сети', 'Переход в социальные сети "Youtube"');
    });

    $('a[href="https://www.facebook.com/avito.business/"]').one('click', function() {
      window.sendClickEvent(landingSlug, 'Переход в социальные сети', 'Переход в социальные сети "Facebook"');
    });

    $('.tn-atom__video-play-icon').one('click', function() {
      window.sendClickEvent(landingSlug, 'Просмотр видео', 'Начало просмотра видео');
    });

    const BottomThreshold = 400
    $(window).on('scroll', function scrollHandler() {
      if ($(document).height() - $(window).scrollTop() - $(window).height() < BottomThreshold) {
        sendScrollToBottomEvent(landingSlug);

        $(window).off('scroll', scrollHandler);
      }
    });
  }
});
