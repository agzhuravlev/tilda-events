$(document).ready(function () {
  const landingSlug = 'b2b_landing_forbusiness';

  if (window.sendPageViewEvent) {
    window.sendPageViewEvent(landingSlug);
  }

  if (window.sendClickEvent) {
    $('a:contains("Выбрать тариф")').first().one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на кнопку', 'Нажатие на кнопку "Выбрать тариф" в первом экране');
    });

    $('a:contains("Выбрать тариф")').last().one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на кнопку', 'Нажатие на кнопку "Выбрать тариф" в последнем экране');
    });

    $('a[href^="https://www.avito.ru/general"]:contains("Подробнее")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Подробнее" в блоке "Товары"');
    });

    $('a[href^="https://business.avito.ru/services"]:contains("Подробнее")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Подробнее" в блоке "Услуги"');
    });

    $('a[href^="https://www.avito.ru/transport"]:contains("Подробнее")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Подробнее" в блоке "Транспорт"');
    });

    $('a[href^="https://www.avito.ru/realestate"]:contains("Подробнее")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Подробнее" в блоке "Недвижимость"');
    });

    $('a[href^="https://www.avito.ru/employer"]:contains("Узнать подробности")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Узнать подробности" в блоке "Нанять сотрудников"');
    });

    $('a[href^="https://business.avito.ru/advice/kak-vybrat-tarif"]:contains("Выбрать тариф")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Выбрать тариф" в блоке "Подключите тариф"');
    });

     $('a:contains("Выбрать услугу продвижения")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Выбрать услуги продвижения" в блоке "Продвигайте объявления"');
    });

     $('a[href^="https://business.avito.ru/avito-pro"]').last().one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Узнать подробности" в блоке "Контролируйте результат"');
    });

    $('a[href^="https://www.avito.ru/moskva/kommercheskaya_nedvizhimost"]').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Снять" в блоке "Снять помещение"');
    });

    $('a[href^="https://www.avito.ru/moskva/oborudovanie_dlya_biznesa"]:contains("Купить")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Купить" в блоке "Оборудовать точку"');
    });

    $('a[href^="https://www.avito.ru/moskva/predlozheniya_uslug"]:contains("Найти")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Найти" в блоке "Найти подрядчиков"');
    });

    $('a[href^="https://www.avito.ru/moskva/gotoviy_biznes"]:contains("Выбрать")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Выбрать" в блоке "Купить готовый бизнес"');
    });

    $('a[href^="https://www.avito.ru/moskva"]:contains("Заказать")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Заказать" в блоке "Закупать расходники"');
    });

    $('a[href^="https://www.avito.ru/additem"]:contains("Распродать")').one('click', function () {
      window.sendClickEvent(landingSlug, 'Нажатие на ссылку', 'Нажатие на ссылку "Распродать" в блоке "Избавляйтесь от ненужного"');
    });
  }

  const BottomThreshold = 400
  $(window).on('scroll', function scrollHandler() {
    if ($(document).height() - $(window).scrollTop() - $(window).height() < BottomThreshold) {
      sendScrollToBottomEvent(landingSlug);

      $(window).off('scroll', scrollHandler);
    }
  });
});

