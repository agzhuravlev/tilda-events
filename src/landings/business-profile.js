$(document).ready(function () {
  if (window.sendPageViewEvent) {
    window.sendPageViewEvent('b2b_landing_business_profile');
  }

  if (window.sendClickEvent) {
    $('a:contains("Настроить бизнес-профиль")').one('click', function() {
      window.sendClickEvent('b2b_landing_business_profile', 'Нажатие на кнопку', 'Нажатие на кнопку "Настроить бизнес-профиль"');
    });

    $('a:contains("Узнать больше о тарифах")').one('click', function() {
      window.sendClickEvent('b2b_landing_business_profile', 'Нажатие на кнопку', 'Нажатие на кнопку "Узнать больше о тарифах"');
    });

    $('a:contains("подключите тариф")').one('click', function() {
      window.sendClickEvent('b2b_landing_business_profile', 'Нажатие на ссылку', 'Нажатие на ссылку "Подключите тариф"');
    });
  }
});
