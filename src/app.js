import ClickStreamClient from '@avito/clickstream-sdk/src/clickstream-sdk';

const b2bLandingPageView = 3596;
const b2bLandingAction = 3597;

const eventCollector = new ClickStreamClient(
  {
    'common': {
      'ua': typeof window !== 'undefined' ? window.navigator.userAgent : ''
    },
    'build': '1',
    'buildUid': '1',
    'softwareVersion': 'tilda',
    'srcId': 95
  },
  'https://cs.avito.ru/clickstream/events/1/json'
);

function sendPageViewEvent(landingSlug) {
  const eventData = {
    url: location.href,
    ref: document.referrer,
    landing_slug: landingSlug
  };

  eventCollector.sendEvent(
    b2bLandingPageView,
    1,
    {
      ...eventData
    }
  );
}

function sendClickEvent(landingSlug, landingAction, actionType) {
  const eventData = {
    url: location.href,
    ref: document.referrer,
    landing_slug: landingSlug,
    landing_action: landingAction,
    action_type: actionType
  };

  eventCollector.sendEvent(
    b2bLandingAction,
    1,
    {
      ...eventData
    }
  );
}


function sendScrollToMiddleEvent(landingSlug) {
  const eventData = {
    url: location.href,
    ref: document.referrer,
    landing_slug: landingSlug,
    landing_action: 'Скролл',
    action_type: 'Скролл до середины страницы'
  };

  eventCollector.sendEvent(
    b2bLandingAction,
    1,
    {
      ...eventData
    }
  );
}


function sendScrollToBottomEvent(landingSlug) {
  const eventData = {
    url: location.href,
    ref: document.referrer,
    landing_slug: landingSlug,
    landing_action: 'Скролл',
    action_type: 'Скролл до конца страницы'
  };

  eventCollector.sendEvent(
    b2bLandingAction,
    1,
    {
      ...eventData
    }
  );
}


window.sendPageViewEvent = sendPageViewEvent;
window.sendClickEvent = sendClickEvent;
window.sendScrollToMiddleEvent = sendScrollToMiddleEvent;
window.sendScrollToBottomEvent = sendScrollToBottomEvent;

