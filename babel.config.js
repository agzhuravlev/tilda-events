module.exports = function(api) {
    api.cache(true);

    return {
        sourceType: 'unambiguous',
        presets: [
            '@babel/react'
        ],
        plugins: [
            '@babel/proposal-class-properties',
            '@babel/plugin-transform-runtime'
        ]
    };
};
